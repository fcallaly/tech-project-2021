# Portfolio Management API Project

## Overview

The primary aim of this hackathon is to create a portfolio management REST API.

This API should allow saving and retrieving records that describe the contents of a financial portofolio.

The secondary aim is to create a web UI that will allow users to see the contents of their portfolio.

If you have web development experience with a framework, you may use that. HOWEVER, clear this with your instructor before proceeding.

If/When you have implemented the above basic system, requirements for further enhancements will be provided. This will included open-ended enhancements whereby you can make use of your particular skills and experience.

## Notes

1. There will be no authentication and a single user is assumed, i.e. there is no requirement to manage users.

2. You should use MongoDB for any persistent storage.

3. You should have a swagger-ui for your REST api.

4. Make good use of git. Use branching and pull requests where you can.

## Technical Getting Started Checklist

1. Create a spring boot project including web & spring-data-mongodb (you might also include lombok if you want to use it).

2. Create a bitbucket repository.

3. Add, commit, push your spring-boot project to the bitbucket repository.

4. Ensure your team has access to the bitbucket repository.

5. Decide on the absolute MINIMUM fields for a first working system e.g. the first version of your model object may just be: String id, String stockTicker, double price, int volume.

If you don't have the above completed in the first 90mins, contact your instructor for help.

## Project Management Getting Started Checklist

1. As a team decide how you will approach the work. E.g. 2 people on Java, 2 people on UI Vs. Everyone on Java until a basic system is working, then move to UI. 

2. Make a task list. Ideally use a tool such as trello to keep track of tasks.

3. Choose the tasks required for a MINIMAL implementation first.

4. Your instructor will drop in for a quick standup within the first hour. Make a note of any questions you have so you can ask them then.

## Suggestions for Success

1. START SMALL. Get a system working that stores a very simple object with minimal fields. You can then enhance to store more complex records.

2. Try pair programming, it can be very effective.

3. Take concious steps to keep a good energy in the team. E.g. give your team a name, systematically plan check-ins with each other.

4. Emphasise quality over quantity.


## End of Day

1. You will be asked to submit the URLs of your bitbucket repositories.

2. You will be asked to give your instructor a quick rundown of the status of your code.

3. You will be asked about your task list. TODO, in progress, done. 

2. You will be asked to give your instructor a short group reflection. What was challenging, what steps you took to deal with those challenges etc.

##
### Appendix A: Notes on Teamwork

It is expected that you work closely as a team during this project.

Your team should be self-organising, but should raise issues with instructors if they are potential blockers to progress. 

Your team should use a task management system such as Trello to keep track of tasks and progress. Divide the work 

Your team should keep track of all source code with git and bitbucket.

You may choose to create a separate bitbucket repository for each component that you tackle e.g. front-end code can be in its own repository. If you create more than one spring-boot application, then each can have its own bitbucket repository. To keep track of your repositories, you can use a single bitbucket 'Project' that each of your repositories is part of.

Your instructor and team members need to access all repositories, so they should be either A) made public OR B) shared with your instructor and all team members.

Throughout your work, you should ensure good communication and organise regular check-ins with each other.

##
### Appendix B: UI Ideas

The screen below might give you some ideas about the type of UI that could be useful. You are NOT expected to implement the screen below exactly as it is shown. This is JUST FOR DEMONSTRATION of the type of thing that COULD be shown.

Just to repeat.... This is NOT what is expected, it is simply here to give ideas!! In the time available, it is understood that your UI will be much simpler.

![Demonstration Portfolio UI](https://www.benivade.com/neueda-training/Tech2020/DemoPortfolioScreen.png)

##
### Appendix C: Useful links

Simple UI that reads live price data from yahoo finance and displays it in a web page: https://bitbucket.org/fcallaly/simple-price-ui